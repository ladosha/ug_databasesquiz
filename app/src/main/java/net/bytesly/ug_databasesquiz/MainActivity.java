package net.bytesly.ug_databasesquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EmployeeDbHelper empDbHelper = new EmployeeDbHelper(this);

        empDbHelper.insert("Vladislav", "Dashtu", "Student", 250);
        empDbHelper.insert("Nika", "Sukhnidze", "Student", 200);
        empDbHelper.insert("Nikoloz", "Katsitadze", "Lecturer", 1000);

        List<Employee> select = empDbHelper.select("Student");
        for (int i = 0; i < select.size(); i++) {
            Log.d("MyEmpList", select.get(i).toString());
        }
    }
}