package net.bytesly.ug_databasesquiz;

public class EmployeeContract {

    public static final String TABLE_NAME = "EMPLOYEES";

    /*
    * private String firstName;
    private String lastName;
    private String position;
    private Integer salary;
    * */

    public static final String ID = "ID";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String POSITION = "position";
    public static final String SALARY = "salary";

}
