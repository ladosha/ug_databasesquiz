package net.bytesly.ug_databasesquiz;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDbHelper extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME = "DB_NAME";
    private static final int VERSION = 1;

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + EmployeeContract.TABLE_NAME + " ("
            + EmployeeContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EmployeeContract.FIRST_NAME + " TEXT, "
            + EmployeeContract.LAST_NAME + " TEXT, "
            + EmployeeContract.POSITION + " TEXT, "
            + EmployeeContract.SALARY + " INTEGER NOT NULL)";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + EmployeeContract.TABLE_NAME;

    public EmployeeDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public void insert(String firstName, String lastName, String position, int salary) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(EmployeeContract.FIRST_NAME, firstName);
        contentValues.put(EmployeeContract.LAST_NAME, lastName);
        contentValues.put(EmployeeContract.POSITION, position);
        contentValues.put(EmployeeContract.SALARY, salary);

        getWritableDatabase().insert(EmployeeContract.TABLE_NAME, null, contentValues);

    }

    public int update(long id, String newPosition, int newSalary) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(EmployeeContract.POSITION, newPosition);
        contentValues.put(EmployeeContract.SALARY, newSalary);

        String where = EmployeeContract.ID + " = ?";
        String[] args = new String[]{
                String.valueOf(id)
        };

        return getWritableDatabase().update(
                EmployeeContract.TABLE_NAME,
                contentValues,
                where,
                args
        );

    }

    public int delete(long carId) {

        String where = EmployeeContract.ID + " = ?";
        String[] args = new String[]{
                String.valueOf(carId)
        };

        return getWritableDatabase().delete(EmployeeContract.TABLE_NAME, where, args);

    }

    public int deleteAll() {
        return getWritableDatabase().delete(EmployeeContract.TABLE_NAME, null, null);
    }

    public List<Employee> select(String position) {

        String[] projection = new String[]{
                EmployeeContract.FIRST_NAME, EmployeeContract.LAST_NAME,
                EmployeeContract.POSITION, EmployeeContract.SALARY
        };

        String where = EmployeeContract.POSITION + " = ?";
        String[] args = new String[]{
                position
        };

        String ordering = EmployeeContract.FIRST_NAME + " ASC";

        @SuppressLint("Recycle") Cursor cursor = getReadableDatabase().query(
                EmployeeContract.TABLE_NAME,
                projection,
                where,
                args,
                null,
                null,
                ordering
        );

        List<Employee> emps = new ArrayList<>();

        while (cursor.moveToNext()) {
            emps.add(new Employee(
                    cursor.getString(cursor.getColumnIndex(EmployeeContract.FIRST_NAME)),
                    cursor.getString(cursor.getColumnIndex(EmployeeContract.LAST_NAME)),
                    cursor.getString(cursor.getColumnIndex(EmployeeContract.POSITION)),
                    cursor.getInt(cursor.getColumnIndex(EmployeeContract.SALARY))
            ));
        }

        return emps;

    }

}
